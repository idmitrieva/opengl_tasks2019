#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <cassert>

#include <iostream>
#include <vector>
#include <random>

#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/LightInfo.hpp"
#include "common/ShaderProgram.hpp"
#include "common/Texture.hpp"

#include "TreeMesh.h"



class SampleApplication : public Application {
public:
    MeshPtr _tree;
    ShaderProgramPtr _shader;

    void makeScene() override {
        Application::makeScene();

        _tree = MakeTreeMesh();
        _tree->setModelMatrix(
            glm::scale(
                glm::mat4(1.0f),
                glm::vec3(0.5f, 0.5f, 0.5f)
            )
        );

        _shader = std::make_shared<ShaderProgram>(
            "597DmitrievaData1/shader.vert",
            "597DmitrievaData1/shader.frag"
        );
    }

    void draw() override {
        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        _shader->use();

        //Загружаем на видеокарту значения юниформ-переменных
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        _shader->setMat4Uniform("modelMatrix", _tree->modelMatrix());

        _tree->draw();

        glUseProgram(0);
    }
};

int main()
{
    SampleApplication app;
    app.start();
    return 0;
}
