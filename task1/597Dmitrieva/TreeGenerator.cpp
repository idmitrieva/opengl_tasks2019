#include "TreeGenerator.h"

State GenerateNewState(const State &state, const glm::vec3 &dir) {

    State new_state;

    new_state.dir = state.dir;
    new_state.pos = state.pos;
    new_state.pos += dir;
    new_state.dir *= 0.9;

    return new_state;
}

glm::vec3 Step(const State& state) {

    float angle = float(rand() % 30 - 15) / 20;
    auto ret = glm::rotateX(state.dir, angle);
    angle = float(rand() % 30 - 15) / 20;
    ret = glm::rotateY(ret, angle);
    angle = float(rand() % 30 - 15) / 20;
    ret = glm::rotateZ(ret, angle);
    return ret;
}

void makeSticks(const State& state, std::vector<TreeBranch>& sticks, int current_level) {

    glm::vec3 d;

    if (current_level == 5) {
        return;
    }

    for (int j = 0; j < 4; j++) {

        d = Step(state);

        State new_state = GenerateNewState(state, d);

        sticks.emplace_back();
        sticks.back().Level = current_level;
        sticks.back().A = state.pos;
        sticks.back().B = state.pos + d;

        makeSticks(new_state, sticks, current_level + 1);
    }
}

std::vector<TreeBranch> generateTreeSticks() {

    State state;
    glm::vec3 d;
    std::vector<TreeBranch> sticks;

    state.pos = glm::vec3(0, 0, 0);
    state.dir = glm::vec3(0, 0, 1);

    int current_level = 0;

    sticks.emplace_back();
    sticks.back().Level = current_level;
    sticks.back().A = state.pos;
    sticks.back().B = state.dir;

    makeSticks(GenerateNewState(state, state.dir), sticks, current_level+1);

    return sticks;
}