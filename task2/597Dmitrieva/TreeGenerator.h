#pragma once
#define GLM_ENABLE_EXPERIMENTAL

#include <vector>
#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>

template<class Point>
struct SegmentT {
    Point A;
    Point B;
};

struct State {
    glm::vec3 pos;
    glm::vec3 dir;
};

using Segment3 = SegmentT<glm::vec3>;

template<class Point>
struct TreeBranchT : public SegmentT<Point> {
    int Level;
};

using TreeBranch = TreeBranchT<glm::vec3>;

std::vector<TreeBranch> generateTreeSticks();
void makeSticks(const State& state, std::vector<TreeBranch>& sticks, int current_level);
glm::vec3 Step(const State& state);
State GenerateNewState(const State &state, const glm::vec3 &dir);
