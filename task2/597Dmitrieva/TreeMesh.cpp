#include "TreeMesh.h"
#include "TreeGenerator.h"

glm::vec3 GetPerpendicular(const glm::vec3& ab) {
    auto p1 = glm::vec3(-ab.y, ab.x, 0);
    if (glm::length(p1) < 1e-5) {
        p1 = glm::vec3(-ab.z, 0, ab.x);
    }
    if (glm::length(p1) < 1e-5) {
        p1 = glm::vec3(0, -ab.z, ab.y);
    }
    assert(glm::length(p1) >= 1e-5);
    return p1;
}

void MakeCone(
        const glm::vec3& a, // центр основания конуса
        const glm::vec3& b, // центр верхушки конуса
        float r1, // радиус основания
        float r2, // радиус верхушки
        int N,
        std::vector<glm::vec3>& vertices,
        std::vector<glm::vec3>& normals,
        std::vector<glm::vec2>& texcoords
) {

    assert(r1 > 0.0 && r2 > 0.0);
    assert(N >= 3);


    auto ab = b - a;

    // перпендикуляры к ab
    auto p1 = normalize(GetPerpendicular(ab));
    auto p2 = normalize(cross(ab, p1));

    auto polar_radius = [&](float phi, float r) {
        return p1 * glm::cos(phi) * r + p2 * glm::sin(phi) * r;
    };

    auto add3Normals = [&](glm::vec3 v) {
        v = normalize(v);
        for (int i = 0; i < 3; ++i) {
            normals.push_back(v);
        }
    };

    auto addVertex = [&](glm::vec3 v, glm::vec3 normal, glm::vec2 tc) {
        vertices.push_back(v);
        normals.push_back(normal);
        texcoords.push_back(tc);
    };

    auto addTriangle = [&](glm::vec3 a, glm::vec3 b, glm::vec3 c) {
        auto normal = normalize(cross(a - b, a - c));
        addVertex(a, normal, {0.0f, 0.0f});
        addVertex(b, normal, {0.0f, 0.0f});
        addVertex(c, normal, {0.0f, 0.0f});
    };

    for (unsigned int i = 0; i < N; ++i) {

        float phi1 = 2.0f * glm::pi<float>() * i / N;
        float phi2 = 2.0f * glm::pi<float>() * (i + 1) / N;

        auto bot1 = a + polar_radius(phi1, r1);
        auto bot2 = a + polar_radius(phi2, r1);

        auto top1 = b + polar_radius(phi1, r2);
        auto top2 = b + polar_radius(phi2, r2);

        // треугольник для основания
        addTriangle(bot2, bot1, a);

        // треугольник для верхушки
        addTriangle(top2, top1, b);

        auto normal = normalize(cross(bot1 - bot2, bot1 - top1));

        // First side triangle
        addVertex(bot1, normal, {0.0f, i / static_cast<float>(N)});
        addVertex(bot2, normal, {0.0f, (i + 1) / static_cast<float>(N)});
        addVertex(top1, normal, {1.0f, (i + 1) / static_cast<float>(N)});

        // Second side triangle
        addVertex(top2, normal, {1.0f, i / static_cast<float>(N)});
        addVertex(top1, normal, {1.0f, (i + 1) / static_cast<float>(N)});
        addVertex(bot2, normal, {0.0f, i / static_cast<float>(N)});
    }
}

void MakeLeaves(
        const glm::vec3& a,
        const glm::vec3& b,
        float size,
        int N,
        std::vector<glm::vec3>& vertices,
        std::vector<glm::vec3>& normals,
        std::vector<glm::vec2>& texcoords
) {
    auto ab = b - a;
    auto nab = normalize(ab);

    // Perpendiculars to ab
    auto p1 = size * normalize(GetPerpendicular(ab));
    auto p2 = size * normalize(cross(ab, p1));
    auto p3 = size * normalize(cross(p1, p2));

    auto addVertex = [&](glm::vec3 v, glm::vec2 tc) {
        vertices.push_back(v);
        normals.push_back(nab);
        texcoords.push_back(tc);
    };

    for (int i = 1; i <= N; ++i) {
        auto c = a + (b - a) * static_cast<float>(i) / static_cast<float>(N);

        // первый квадрат
        addVertex(c - p1 - p2, {0.0, 0.0});
        addVertex(c - p1 + p2, {0.0, 1.0});
        addVertex(c + p1 + p2, {1.0, 1.0});

        addVertex(c + p1 - p2, {1.0, 0.0});
        addVertex(c - p1 - p2, {0.0, 0.0});
        addVertex(c + p1 + p2, {1.0, 1.0});

        // второй квадрат, перпендикулярный первому
        addVertex(c - p1 - p3, {0.0, 0.0});
        addVertex(c - p1 + p3, {0.0, 1.0});
        addVertex(c + p1 + p3, {1.0, 1.0});

        addVertex(c + p1 - p3, {1.0, 0.0});
        addVertex(c - p1 - p3, {0.0, 0.0});
        addVertex(c + p1 + p3, {1.0, 1.0});
    }
}

MeshPtr MakeMesh(
        const std::vector<glm::vec3>& vertices,
        const std::vector<glm::vec3>& normals,
        const std::vector<glm::vec2>& texcoords
) {

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);

    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}

std::pair<MeshPtr, MeshPtr> MakeTreeMesh() {
    constexpr int DETAIL = 40;
    constexpr int MIN_LEAF_LEVEL = 3;
    constexpr float LEAF_RECT_SIZE = 0.12;

    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    std::vector<glm::vec3> lvertices;
    std::vector<glm::vec3> lnormals;
    std::vector<glm::vec2> ltexcoords;

    std::vector<TreeBranch> sticks = generateTreeSticks();

    auto get_radius = [](int level) {
        constexpr float BARK_RADIUS = 0.14f;
        constexpr float INITIAL_RADIUS = 0.08f;
        constexpr float RADIUS_SCALE = 0.4f;

        float radius = level == 0 ? BARK_RADIUS : INITIAL_RADIUS;
        for (int i = 0; i < level - 1; i++) {
            radius *= RADIUS_SCALE;
        }
        return radius;
    };

    for (auto& stick : sticks) {
        MakeCone(
                stick.A,
                stick.B,
                get_radius(stick.Level),
                get_radius(stick.Level + 1),
                DETAIL,
                vertices,
                normals,
                texcoords
        );
        if (stick.Level >= MIN_LEAF_LEVEL) {
            MakeLeaves(
                    stick.A, stick.B,
                    LEAF_RECT_SIZE, 3,
                    lvertices, lnormals, ltexcoords
            );
        }
    }

    return {
            MakeMesh(vertices, normals, texcoords),
            MakeMesh(lvertices, lnormals, ltexcoords)
    };
}


