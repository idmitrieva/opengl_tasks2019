#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <cassert>

#include <iostream>
#include <vector>
#include <random>

#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/LightInfo.hpp"
#include "common/ShaderProgram.hpp"
#include "common/Texture.hpp"

#include "TreeMesh.h"



class SampleApplication : public Application {
public:
    MeshPtr _tree;
    MeshPtr _leaves;
    MeshPtr _marker; //Маркер для источника света

    ShaderProgramPtr _shader;
    ShaderProgramPtr _markerShader;


    // Угол направленного источника света
    float _phi = glm::pi<float>() * 0.0f;
    float _theta = glm::pi<float>() * 0.25f;

    // Параметры направленного источника света
    LightInfo _light;

    TexturePtr _barkTexture;
    TexturePtr _leafTexture;

    GLuint _sampler;
    GLuint _leafSampler;

    void makeScene() override {
        Application::makeScene();

        std::tie(_tree, _leaves) = MakeTreeMesh();

        auto modelMatrix = glm::scale(
                glm::mat4(1.0f),
                glm::vec3(0.5f, 0.5f, 0.5f)
        );

        _tree->setModelMatrix(modelMatrix);
        _leaves->setModelMatrix(modelMatrix);

        _marker = makeSphere(0.1f);


        //=========================================================

        //Инициализация шейдеров

        _shader = std::make_shared<ShaderProgram>(
                "597DmitrievaData2/shader.vert",
                "597DmitrievaData2/shader.frag"
        );

        _markerShader = std::make_shared<ShaderProgram>("597DmitrievaData2/marker.vert", "597DmitrievaData2/marker.frag");


        //=========================================================

        //Инициализация значений переменных освщения
        // Direction pointing outwards, toward the light source
//        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_theta), glm::sin(_phi) * glm::cos(_theta));
        _light.position = glm::vec3(6.0, 0.0, 1.5);

        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(0.3, 0.3, 0.3);

        //=========================================================

        //Загрузка и создание текстур
        _barkTexture = loadTexture("597DmitrievaData2/bark.png");
        _leafTexture = loadTexture("597DmitrievaData2/leaves2.png");

        //=========================================================
        //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
        for (auto sampler : {_sampler, _leafSampler}) {
            glGenSamplers(1, &sampler);
            glSamplerParameteri(sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            glSamplerParameteri(sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glSamplerParameteri(sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
            glSamplerParameteri(sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
        }

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    }

    void draw() override {
        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        _shader->use();

        //Загружаем на видеокарту значения юниформ-переменных
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);


        glm::vec3 lightDirCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 0.0));
        _shader->setVec3Uniform("light.dir", lightDirCamSpace); //копируем положение уже в системе виртуальной камеры
        _shader->setVec3Uniform("light.La", _light.ambient);
        _shader->setVec3Uniform("light.Ld", _light.diffuse);
        _shader->setVec3Uniform("light.Ls", _light.specular);

        auto drawMesh = [&](const MeshPtr& mesh, const TexturePtr& tx, GLuint sampler, bool isLeaf) {
            glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
            glBindSampler(0, sampler);
            tx->bind();
            _shader->setIntUniform("diffuseTex", 0);

            //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
            _shader->setMat4Uniform("modelMatrix", mesh->modelMatrix());
            _shader->setMat3Uniform("normalToCameraMatrix",glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * mesh->modelMatrix()))));
            _shader->setIntUniform("isLeaf", isLeaf);
            mesh->draw();
        };

        drawMesh(_tree, _barkTexture, _sampler, false);
        drawMesh(_leaves, _leafTexture, _leafSampler, true);

        _tree->draw();
        _leaves->draw();


        //Рисуем маркеры для всех источников света
        {
            _markerShader->use();

            _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light.position));
            _markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
            _marker->draw();
        }

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);

    }
};

int main()
{
    SampleApplication app;
    app.start();
    return 0;
}
