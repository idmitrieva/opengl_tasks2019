#pragma once
#define GLM_ENABLE_EXPERIMENTAL

#include <vector>
#include "common/Mesh.hpp"

std::pair<MeshPtr, MeshPtr> MakeTreeMesh();

MeshPtr MakeMesh(const std::vector<glm::vec3>& vertices, const std::vector<glm::vec3>& normals, const std::vector<glm::vec2>& texcoords);

void MakeCone(
        const glm::vec3& a,
        const glm::vec3& b,
        float r1,
        float r2,
        int N,
        std::vector<glm::vec3>& vertices,
        std::vector<glm::vec3>& normals,
        std::vector<glm::vec2>& texcoords
);
